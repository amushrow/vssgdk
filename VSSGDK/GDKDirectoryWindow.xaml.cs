﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualStudio.PlatformUI;
using System.Diagnostics;


using System.Text.RegularExpressions;
using System.IO;
using Microsoft.VisualStudio.TemplateWizard;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio;
using EnvDTE;
using System.Runtime.InteropServices;

namespace VSSGDK
{
	/// <summary>
	/// Interaction logic for GDKDirectoryWindow.xaml
	/// </summary>
	public partial class GDKDirectoryWindow : DialogWindow
	{
		public GDKDirectoryWindow()
		{
			InitializeComponent();
		}

		private bool IsValidSGDKDir(string path)
		{
			try
			{
				string RescompPath = System.IO.Path.Combine(path, "bin\\rescomp.jar");
				return File.Exists(RescompPath);
			}
			catch(ArgumentException)
			{
				//Not even a valid path to begin with
				return false;
			}
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			System.Diagnostics.Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
			e.Handled = true;
		}

		private void BrowseButton_Click(object sender, RoutedEventArgs e)
		{
			ThreadHelper.ThrowIfNotOnUIThread();

            var uiShell = Package.GetGlobalService(typeof(IVsUIShell)) as IVsUIShell;
			IntPtr dirNameHandle = IntPtr.Zero;

			try
			{
				IntPtr windowHandle;
				uiShell.GetDialogOwnerHwnd(out windowHandle);

				VSBROWSEINFOW[] browseInfoArray = new VSBROWSEINFOW[1];
				browseInfoArray[0].lStructSize = (uint)Marshal.SizeOf(browseInfoArray[0]);
				browseInfoArray[0].hwndOwner = windowHandle;
				browseInfoArray[0].nMaxDirName = 2048;
				browseInfoArray[0].pwzDlgTitle = "Locate SGDK Directory";
				browseInfoArray[0].pwzInitialDir = null;
				dirNameHandle = Marshal.AllocCoTaskMem(2 * (int)browseInfoArray[0].nMaxDirName);
				browseInfoArray[0].pwzDirName = dirNameHandle;

				int hr = uiShell.GetDirectoryViaBrowseDlg(browseInfoArray);

				switch (hr)
				{
					case VSConstants.S_OK:
						SGDKDir.Text = Marshal.PtrToStringAuto(dirNameHandle);
						break;

					case VSConstants.OLE_E_PROMPTSAVECANCELLED:
						// Cancelled
						break;
				}
			}
			catch(Exception)
			{
				//Balls
			}
			finally
			{
				if (dirNameHandle != IntPtr.Zero)
				{
					Marshal.FreeCoTaskMem(dirNameHandle);
				}
			}
        }

		private void OKButton_Click(object sender, RoutedEventArgs e)
		{
			if (IsValidSGDKDir(SGDKDir.Text))
			{
				//Set the environment variable
				Environment.SetEnvironmentVariable("GDK", SGDKDir.Text, EnvironmentVariableTarget.User);

				//Also set the variable for the current process, since the change to the user environment may not be picked up right away
				Environment.SetEnvironmentVariable("GDK", SGDKDir.Text, EnvironmentVariableTarget.Process);

				Close();
			}
		}
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void SGDKDir_TextChanged(object sender, TextChangedEventArgs e)
		{
			OKButton.IsEnabled = IsValidSGDKDir(SGDKDir.Text);
		}
	}
}
