﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.VisualStudio.TemplateWizard;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio;
using EnvDTE;
using System.Runtime.InteropServices;
using System.Drawing;
using Microsoft.VisualStudio.Package;

namespace VSSGDK
{
	class SGDKProjectWizard : IWizard
	{
        // This method is called before opening any item that
        // has the OpenInEditor attribute.
        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectFinishedGenerating(Project project)
        {
            
        }

        // This method is only called for item templates,
        // not for project templates.
        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
        }

        // This method is called after the project is created.
        public void RunFinished()
        {
        }

        public void RunStarted(object automationObject,
            Dictionary<string, string> replacementsDictionary,
            WizardRunKind runKind, object[] customParams)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            string GDKDir = Environment.GetEnvironmentVariable("GDK");
            if (GDKDir == null || GDKDir == string.Empty)
            {
                GDKDirectoryWindow GDKDialog = new GDKDirectoryWindow();
                GDKDialog.ShowModal();
            }

            var desiredNamespace = "SGDKProject";
            replacementsDictionary.TryGetValue("$safeprojectname$", out desiredNamespace);

            var templatePath = (customParams.GetLength(0) > 0) ? Path.GetDirectoryName((string)customParams[0]) : string.Empty;

            var solutionDir = string.Empty;
            replacementsDictionary.TryGetValue("$solutiondirectory$", out solutionDir);

            var projectDir = solutionDir;
            replacementsDictionary.TryGetValue("$destinationdirectory$", out projectDir);

            bool singleProjectSolution = solutionDir == projectDir;

            // If no solution name give, we take the project's safe name
            if (!replacementsDictionary.ContainsKey("$specifiedsolutionname$"))
            {
                replacementsDictionary["$specifiedsolutionname$"] = desiredNamespace;
            }

            if (singleProjectSolution)
			{
                replacementsDictionary["$debugprojecttarget$"] = String.Format("{0}.elf", desiredNamespace);
                replacementsDictionary["$projectsubdir$"] = "/";
            }
            else
            {
                replacementsDictionary["$debugprojecttarget$"] = String.Format("{0}.elf ({0}\\\\{0}.elf)", desiredNamespace);
                replacementsDictionary["$projectsubdir$"] = String.Format("/{0}/", desiredNamespace);
            }

            if (singleProjectSolution)
            {
                this.AddTemplateFile(
                    Path.Combine(templatePath, "CMakeLists_aio.txt"),
                    Path.Combine(solutionDir, "CMakeLists.txt"),
                    replacementsDictionary);
            }
            else
            {
                this.AddTemplateFile(
                    Path.Combine(templatePath, "CMakeLists_topLevel.txt"),
                    Path.Combine(solutionDir, "CMakeLists.txt"),
                    replacementsDictionary);

                this.AddTemplateFile(
                    Path.Combine(templatePath, "CMakeLists_project.txt"),
                    Path.Combine(projectDir, "CMakeLists.txt"),
                    replacementsDictionary);
            }

            //Solution files
            this.AddDirectory(
                new DirectoryInfo(Path.Combine(templatePath, ".vs")),
                new DirectoryInfo(Path.Combine(solutionDir, ".vs")),
                replacementsDictionary, true);

            this.AddTemplateFile(
                Path.Combine(templatePath, "CMakeSettings.json"),
                Path.Combine(solutionDir, "CMakeSettings.json"),
                replacementsDictionary);

			this.AddTemplateFile(
				Path.Combine(templatePath, "sgdk_toolchain.cmake"),
				Path.Combine(solutionDir, "sgdk_toolchain.cmake"),
				replacementsDictionary);

			//Project files
			this.AddTemplateFile(
                    Path.Combine(templatePath, "VSWorkspaceSettings.json"),
                    Path.Combine(projectDir, "VSWorkspaceSettings.json"),
                    replacementsDictionary);

            this.AddDirectory(
                new DirectoryInfo(Path.Combine(templatePath, "src")),
                new DirectoryInfo(Path.Combine(projectDir, "src")),
                replacementsDictionary, true);

            this.AddDirectory(
                new DirectoryInfo(Path.Combine(templatePath, "res")),
                new DirectoryInfo(Path.Combine(projectDir, "res")),
                replacementsDictionary, true);

            this.AddDirectory(
                new DirectoryInfo(Path.Combine(templatePath, "inc")),
                new DirectoryInfo(Path.Combine(projectDir, "inc")),
                replacementsDictionary, true);

            this.AddDirectory(
                new DirectoryInfo(Path.Combine(templatePath, "_gens")),
                new DirectoryInfo(Path.Combine(projectDir, "_gens")),
                replacementsDictionary, false);

			this.AddDirectory(
				new DirectoryInfo(Path.Combine(templatePath, "toolchain")),
				new DirectoryInfo(Path.Combine(solutionDir, "toolchain")),
				replacementsDictionary, false);

			var vsSolution = Package.GetGlobalService(typeof(SVsSolution)) as IVsSolution7;
            if (vsSolution != null)
            {
                vsSolution.OpenFolder(solutionDir);
            }
        }

        // This method is only called for item templates,
        // not for project templates.
        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        protected void AddTemplateFile(string source, string target, Dictionary<string, string> replacementsDictionary)
        {
            if (System.IO.File.Exists(source))
            {
                // see https://stackoverflow.com/questions/1231768/c-sharp-string-replace-with-dictionary
                Regex re = new Regex(@"(\$\w+\$)", RegexOptions.Compiled);

                string input = ReadFileWithIncludes(source);
                string output = re.Replace(input, match => replacementsDictionary[match.Groups[1].Value]);

                // Don't overwrite
                if (!File.Exists(target))
                {
                    System.IO.File.WriteAllText(target, output);
                }
            }
        }

		protected string ReadFileWithIncludes(string source)
        {
            if (System.IO.File.Exists(source))
            {
                string input = System.IO.File.ReadAllText(source);

				Regex includeRegex = new Regex("\\$include \"(.*?)\"\\$", RegexOptions.Compiled);
				foreach (Match include in includeRegex.Matches(input))
                {
                    string relativeIncludePath = include.Groups[1].Value;
                    string includePath = Path.IsPathRooted(relativeIncludePath) ? relativeIncludePath : Path.Combine(Path.GetDirectoryName(source), relativeIncludePath);

                    // Read in the include file
					string includeText = ReadFileWithIncludes(includePath);

                    // Replace all instances of the inlcude with the whole file
                    input = input.Replace(include.Groups[0].Value, includeText);

                }
				return input;
			}

            return "";
		}


		protected void AddDirectory(DirectoryInfo source, DirectoryInfo target, Dictionary<string, string> replacementsDictionary, bool templated)
        {
           
            if (source.FullName.ToLower() == target.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it.
            if (!target.Exists)
            {
                target.Create();
            }

            if (!source.Exists)
            {
                return;
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                if (templated)
                {
                    AddTemplateFile(fi.FullName, Path.Combine(target.ToString(), fi.Name), replacementsDictionary);
                }
                else
                {
                    fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
                }
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                AddDirectory(diSourceSubDir, nextTargetSubDir, replacementsDictionary, templated);
            }
        }
    }
}
