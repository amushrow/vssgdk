#include "genesis.h"


int main(bool hardReset)
{
    int count = 0;

    while(TRUE)
    {
        if (count < 60)
        {
            VDP_drawText("Hello       ", 12, 12);
        }
        else
        {
            VDP_drawText("      World!", 12, 12);
        }

        ++count;
        if (count >= 120)
            count = 0;

        // always call this method at the end of the frame
        SYS_doVBlankProcess();
    }

    return 0;
}
