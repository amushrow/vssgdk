# determine the compiler to use for GDK resource files

if(NOT CMAKE_GDKRES_COMPILER)
    find_program(CMAKE_GDKRES_COMPILER "java")
endif()

mark_as_advanced(CMAKE_RC_COMPILER)
set(CMAKE_GDKRES_OUTPUT_EXTENSION .o)

# configure variables set in this file for fast reload later on
configure_file(${CMAKE_CURRENT_LIST_DIR}/CMakeGDKRESCompiler.cmake.in
               ${CMAKE_PLATFORM_INFO_DIR}/CMakeGDKRESCompiler.cmake)

set(CMAKE_GDKRES_COMPILER_ENV_VAR "GDKRES")
