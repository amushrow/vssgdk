# determine the compiler to use for GDK resource files

file(TO_CMAKE_PATH "$ENV{GDK}" ENV_GDK)

if(NOT CMAKE_Z80_COMPILER)
    set (CMAKE_Z80_COMPILER "${ENV_GDK}/bin/sjasm.exe")
endif()

set(CMAKE_Z80_OUTPUT_EXTENSION .o)

# configure variables set in this file for fast reload later on
configure_file(${CMAKE_CURRENT_LIST_DIR}/CMakeZ80Compiler.cmake.in
               ${CMAKE_PLATFORM_INFO_DIR}/CMakeZ80Compiler.cmake)

set(CMAKE_Z80_COMPILER_ENV_VAR "z80")
