
file(TO_CMAKE_PATH "$ENV{GDK}" ENV_GDK)

# Generate files (header, m68k assembler, dependency)
get_filename_component(OUTPUT_DIRECTORY ${Object} DIRECTORY)
get_filename_component(OUTPUT_NAME ${Object} NAME_WE)
execute_process(COMMAND ${RESCompiler} -jar "${ENV_GDK}/bin/rescomp.jar" ${Source} "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.s"  -dep)

# Compile m68k assembly
string(REPLACE " " ";" FLAGS_LIST ${Flags})
execute_process(COMMAND ${ASMCompiler} -x assembler-with-cpp "-B${ENV_GDK}/bin" ${FLAGS_LIST} -c "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.s" -o ${Object})

# Move header to source directory
get_filename_component(SOURCE_DIRECTORY ${Source} DIRECTORY)
file(RENAME "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.h" "${SOURCE_DIRECTORY}/${OUTPUT_NAME}.h")

# Move dependency file to where cmake wants
file(RENAME "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.d" "${DepFile}")

# Clean up intermediate files
file(REMOVE "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.s")