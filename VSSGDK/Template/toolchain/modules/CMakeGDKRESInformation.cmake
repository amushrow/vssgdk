# This file sets the basic flags for the GDK Resource Compiler.

# make sure we don't use CMAKE_BASE_NAME from somewhere else
set(CMAKE_BASE_NAME "rescomp")

set(CMAKE_GDKRES_FLAGS_INIT "-m68000 -Wall -Wextra -Wno-shift-negative-value -Wno-main -Wno-unused-parameter -fno-builtin")

cmake_initialize_per_config_variable(CMAKE_GDKRES_FLAGS "Flags for GDK Resource Compiler")

set(CMAKE_GDKRES_DEPFILE_FORMAT gcc)
set(CMAKE_GDKRES_DEPENDS_USE_COMPILER TRUE)

file(TO_CMAKE_PATH "$ENV{GDK}" ENV_GDK)

# compile a Resource file into an object file
if(NOT CMAKE_GDKRES_COMPILE_OBJECT)
    set(CMAKE_GDKRES_COMPILE_OBJECT "<CMAKE_COMMAND> -DRESCompiler=\"<CMAKE_GDKRES_COMPILER>\" -DASMCompiler=\"<CMAKE_ASM_COMPILER>\" -DSource=\"<SOURCE>\" -DObject=\"<OBJECT>\" -DFlags=\"<FLAGS>\" -DDepFile=\"<DEP_FILE>\" -P ${CMAKE_CURRENT_LIST_DIR}/CMakeGDKRESCompileScript.cmake")
endif()

if(NOT CMAKE_GDKRES_ARCHIVE_CREATE)
    set(CMAKE_GDKRES_ARCHIVE_CREATE ${CMAKE_C_ARCHIVE_CREATE}) #Same as C
endif()

if(NOT CMAKE_GDKRES_ARCHIVE_FINISH)   
    set(CMAKE_GDKRES_ARCHIVE_FINISH ${CMAKE_C_ARCHIVE_FINISH}) #Same as C
endif()

# set this variable so we can avoid loading this more than once.
set(CMAKE_GDKRES_INFORMATION_LOADED 1)
