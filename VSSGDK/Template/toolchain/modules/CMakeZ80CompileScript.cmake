
file(TO_CMAKE_PATH "$ENV{GDK}" ENV_GDK)

# Generate Z80 object file
string(REPLACE " " ";" INCLUDES_LIST ${Includes})
execute_process(COMMAND ${Z80Compiler} -q ${INCLUDES_LIST} ${Source} "${Object}.o80")

# Embed Z80 object file into m68k assembly and generate header
get_filename_component(OUTPUT_DIRECTORY ${Object} DIRECTORY)
get_filename_component(OUTPUT_NAME ${Object} NAME_WE)
execute_process(COMMAND ${ENV_GDK}/bin/bintos "${Object}.o80" "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.s")

# Compile m68k assembly
string(REPLACE " " ";" FLAGS_LIST ${Flags})
execute_process(COMMAND ${ASMCompiler} -x assembler-with-cpp "-B${ENV_GDK}/bin" ${FLAGS_LIST} -c "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.s" -o ${Object})

# Move header to source directory
get_filename_component(SOURCE_DIRECTORY ${Source} DIRECTORY)
file(RENAME "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.h" "${SOURCE_DIRECTORY}/${OUTPUT_NAME}.h")

# Clean up intermediate files
file(REMOVE "${Object}.o80")
file(REMOVE "${OUTPUT_DIRECTORY}/${OUTPUT_NAME}.s")