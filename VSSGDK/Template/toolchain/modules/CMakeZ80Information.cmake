# This file sets the basic flags for the GDK Resource Compiler.

# make sure we don't use CMAKE_BASE_NAME from somewhere else
set(CMAKE_BASE_NAME "z80")

file(TO_CMAKE_PATH "$ENV{GDK}" ENV_GDK)

set(CMAKE_Z80_FLAGS_INIT "")
set(CMAKE_INCLUDE_FLAG_Z80 "-I")
set(CMAKE_DEPFILE_FLAGS_Z80 "-MD -MF <DEP_FILE>")

cmake_initialize_per_config_variable(CMAKE_Z80_FLAGS "Flags for GDK Z80 Compiler")

set(CMAKE_Z80_DEPFILE_FORMAT gcc)
set(CMAKE_Z80_DEPENDS_USE_COMPILER TRUE)

# compile a Resource file into an object file
if(NOT CMAKE_Z80_COMPILE_OBJECT)   
    set(CMAKE_Z80_COMPILE_OBJECT "<CMAKE_COMMAND> -DZ80Compiler=\"<CMAKE_Z80_COMPILER>\" -DASMCompiler=\"<CMAKE_ASM_COMPILER>\" -DIncludes=\"<INCLUDES>\" -DSource=\"<SOURCE>\" -DObject=\"<OBJECT>\" -DFlags=\"<FLAGS>\" -P ${CMAKE_CURRENT_LIST_DIR}/CMakeZ80CompileScript.cmake")
endif()

if(NOT CMAKE_Z80_ARCHIVE_CREATE)
    set(CMAKE_Z80_ARCHIVE_CREATE ${CMAKE_C_ARCHIVE_CREATE}) #Same as C
endif()

if(NOT CMAKE_Z80_ARCHIVE_FINISH)   
    set(CMAKE_Z80_ARCHIVE_FINISH ${CMAKE_C_ARCHIVE_FINISH}) #Same as C
endif()

# set this variable so we can avoid loading this more than once.
set(CMAKE_Z80_INFORMATION_LOADED 1)
