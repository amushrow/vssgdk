cmake_minimum_required (VERSION 3.8)

set(CMAKE_SYSTEM_NAME "Generic-ELF")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_LIST_DIR}/modules/")

file(TO_CMAKE_PATH "$ENV{GDK}" ENV_GDK)

set(CMAKE_C_FLAGS "-m68000 -Wall -Wextra -Wno-shift-negative-value -Wno-main -Wno-unused-parameter -fno-builtin")
set(CMAKE_C_FLAGS_DEBUG "-O0 -ggdb -DDEBUG=1")
set(CMAKE_C_FLAGS_RELEASE "-O3 -fuse-linker-plugin -fno-web -fno-gcse -fno-unit-at-a-time -fomit-frame-pointer -flto")
set(CMAKE_C_LINK_FLAGS "-nostdlib -Wl,--gc-sections")

set(CMAKE_C_DEPFILE_FORMAT gcc)
set(CMAKE_C_DEPENDS_USE_COMPILER TRUE)
set(CMAKE_DEPFILE_FLAGS_C "-MD -MT <DEP_TARGET> -MF <DEP_FILE>")

set(CMAKE_C_COMPILER "${ENV_GDK}/bin/gcc.exe")
set(CMAKE_C_COMPILE_OBJECT "<CMAKE_C_COMPILER> -B${ENV_GDK}/bin <FLAGS> <INCLUDES> -c <SOURCE> -o <OBJECT>")
set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> -B${ENV_GDK}/bin -n -T ${ENV_GDK}/md.ld <OBJECTS> <LINK_LIBRARIES> -o <TARGET> <CMAKE_C_LINK_FLAGS>")

set(CMAKE_ASM_COMPILE_OBJECT "<CMAKE_ASM_COMPILER> -x assembler-with-cpp -B${ENV_GDK}/bin <FLAGS> <INCLUDES> -c <SOURCE> -o <OBJECT>")

set(CMAKE_CXX_COMPILER "")