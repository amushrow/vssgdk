﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Shell.Events;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using System.IO;

namespace VSSGDK
{
	[PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
	[Guid(VSPackage.PackageGuidString)]
	[ProvideAutoLoad(VSConstants.UICONTEXT.NoSolution_string, PackageAutoLoadFlags.BackgroundLoad)]
	public sealed class VSPackage : AsyncPackage
	{
		/// <summary>
		/// VSPackage GUID string.
		/// </summary>
		public const string PackageGuidString = "cd531c0a-0de9-421b-a279-0384bf232065";

		/// <summary>
		/// Initializes a new instance of the <see cref="VSPackage"/> class.
		/// </summary>
		public VSPackage()
		{
			
		}

		#region Package Members

		protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
		{
			// Since this package might not be initialized until after a solution has finished loading,
			// we need to check if a solution has already been loaded and then handle it.
			bool isFolderOpen = await IsFolderOpenAsync();
			if (isFolderOpen)
			{
				HandleOpenFolder();
			}

			// Listen for subsequent solution events
			SolutionEvents.OnAfterOpenFolder += HandleOpenFolder;
		}

		private async Task<bool> IsFolderOpenAsync()
		{
			await JoinableTaskFactory.SwitchToMainThreadAsync();
			var solService = await GetServiceAsync(typeof(SVsSolution)) as IVsSolution;

			string solutionDir;
			string solutionFile;
			string userOpts;
			//Is there an better way to verify if we have a Folder open (instead of an *.sln)? Maybe
			if (solService != null && solService.GetSolutionInfo(out solutionDir, out solutionFile, out userOpts) == VSConstants.S_OK)
			{
				if (solutionDir != null && solutionFile != null && solutionDir != string.Empty)
				{
					solutionDir = Path.GetFullPath(solutionDir.TrimEnd(Path.DirectorySeparatorChar));
					solutionFile = Path.GetFullPath(solutionFile);
					return Path.Equals(solutionDir, solutionFile);
				}
			}

			return false;
		}

		private void HandleOpenFolder(object sender = null, EventArgs e = null)
		{
			string ExtensionPath = Path.Combine(Path.GetDirectoryName(GetType().Assembly.Location), "Binaries");
			Environment.SetEnvironmentVariable("VSSGDK_BIN", ExtensionPath);
		}

		#endregion
	}
}
